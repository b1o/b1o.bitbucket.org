# Tanks

Tank game made with phaser.js

# Update 08.02.2015/6:35AM

* Added health bars.
* Added bots bullet detection. Still have to be properly implemented.
* Bots are now suicidal.
* Changed the earth sprite.
* Added backwards movement.