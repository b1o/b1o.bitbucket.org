var land;
var myId = 0;
var enemies;
var enemyBullets;
var enemiesTotal = 15;
var enemiesAlive = 0;
var explosions;
var cursors;
var logo;
var alive = true;
var tanksList = [];
var bullets;
var bullet;
var nextFire = 0;

var ready = false;
var eurecaServer;

var eurecaClientSetup = function() {
    //create an instance of eureca.io client
    var eurecaClient = new Eureca.Client();
    
    eurecaClient.ready(function (proxy) {       
        eurecaServer = proxy;
    });
    
    
    //methods defined under "exports" namespace become available in the server side
    
    eurecaClient.exports.setId = function(id) 
    {
        //create() is moved here to make sure nothing is created before uniq id assignation
        console.log("creating world");
        myId = id;
        create();
        eurecaServer.handshake();
        ready = true;
    }   
    
    eurecaClient.exports.kill = function(id)
    {   
        for (i in tanksList) {
            if(tanksList[i].id == id) {
                tanksList[i].kill();
                console.log('killing ', id, tanksList[id]);
            }
        }
    }   
    
    eurecaClient.exports.spawnEnemy = function(i, x, y)
    {
        
        if (i == myId) return; //this is me
        
        console.log('SPAWN');
        var tnk = new PlayerTank(i, game);
        tanksList.push(tnk);
    }
    
    
    eurecaClient.exports.updateState = function(id, state)
    {
        for(i in tanksList)  {
            if(tanksList[i].id == id){
                tanksList[i].cursor = state;
                tanksList[i].tank.x = state.x;
                tanksList[i].tank.y = state.y;
                tanksList[i].tank.angle = state.angle;
                tanksList[i].turret.rotation = state.rot;
                tanksList[i].update();
            }
        }
    }
}

Tank = function (index, game, player, bullets) {
    var x = game.world.randomX;
    var y = game.world.randomY;


    this.game = game;
    this.health = 3;
    this.maxHealth = 3;
    this.enemy = player.tank;
    
    this.bullets = bullets;
    this.fireRate = 1000;
    this.nextFire = 0;
    this.alive = true;
    this.currentSpeed = 100;
    this.timer = game.time.create(false);
    this.TURN_RATE = 4;
    this.turretTurnRate = 8;
    this.targetAngle = false;
    this.signal = false;
    this.rect = game.add.sprite(x, y, 'radar');

    this.shadow = game.add.sprite(x, y, 'enemy', 'shadow');
    this.tank = game.add.sprite(x, y, 'enemy', 'tank1');
    this.turret = game.add.sprite(x, y, 'enemy', 'turret');

    this.shadow.anchor.set(0.5);
    this.tank.anchor.set(0.5);
    this.turret.anchor.set(0.3, 0.5);

    game.physics.enable(this.rect, Phaser.Physics.ARCADE);  
    game.physics.enable(this.tank, Phaser.Physics.ARCADE);

    this.rect.anchor.setTo(0.5, 0.5);
    this.rect.width = 250;
    this.rect.height = 250;
    this.rect.collideWorldBounds = true;
    this.rect.renderable = false;

    this.tank.immovable = false;
    this.tank.collideWorldBounds = true;
    this.tank.body.bounce.setTo(1, 1);
    this.tank.body.drag.set(0.2);
    this.tank.body.collideWorldBounds = true;
    this.tank.name = index.toString();
    this.style = { font: "15px Arial", fill: "#ff0044", wordWrap: true, wordWrapWidth: this.tank.width, align: "center" };
    this.hpbar = game.add.text(0, 0, this.health.toString() + ' / ' + this.maxHealth.toString(), this.style);
    this.hpbar.anchor.set(1, 1.5);

    this.explosions = game.add.group();

    for (var i = 0; i < 10; i++)
    {
        var explosionAnimation = this.explosions.create(0, 0, 'kaboom', [0], false);
        explosionAnimation.anchor.setTo(0.5, 0.5);
        explosionAnimation.animations.add('kaboom');
    }
   
    
    game.time.events.loop(2000, this.turn, this);
};




Tank.prototype.radar = function() {
    if(this.game.physics.arcade.distanceBetween(this.tank, this.enemy) < 250) {

        return true, this.tank.x, this.tank.y;
    }
    return false;
}

Tank.prototype.dodge = function() {
    this.targetAngle = Math.atan2(this.tank.y - this.enemy.y, this.tank.x, this.enemy.y);

    if (this.tank.rotation !== this.targetAngle) {
        var delta = this.targetAngle - this.tank.rotation;

        if (delta > Math.PI) delta -= Math.PI * 2;
        if (delta < -Math.PI) delta += Math.PI * 2;
        
        if (delta > 0) {
            // Turn clockwise
            this.tank.angle += this.TURN_RATE;
        } else {
            // Turn counter-clockwise
            this.tank.angle -= this.TURN_RATE;
        }
    }
}

Tank.prototype.increment = function(value){
    this.tank.angle += value;
    //console.log(this.index, this.tank.angle);
};

Tank.prototype.decrement = function(value){
    this.tank.angle -= value;
    //console.log(this.index, this.tank.angle);
};

Tank.prototype.stopTimer = function() {
    this.timer.stop();
    //this.done = true;
};

Tank.prototype.turn = function(direction) {
    if(direction === undefined) direction = Math.floor((Math.random() * 2) + 1);
    var seconds = Math.floor((Math.random() * 700) + 500);

    if (direction === 2) {
        this.timer.loop(10, this.increment, this, 4);
        this.timer.start();
        this.timer.add(seconds, this.stopTimer, this);
    } else {     
        this.timer.loop(10, this.decrement, this, 4);
        this.timer.start();
        this.timer.add(seconds, this.stopTimer, this);
    }
};


Tank.prototype.damage = function() {
    this.health -= 1;
    this.hpbar.destroy();
    
    this.hpbar = game.add.text(0, 0, this.health.toString() + ' / ' + this.maxHealth.toString(), this.style);
    this.hpbar.anchor.set(1, 1.5);
    if (this.health <= 0) {
        this.alive = false;

        this.shadow.kill();
        this.tank.kill();
        this.turret.kill();
        this.hpbar.destroy();
        return true;
    }
    return false;
}

Tank.prototype.turnAround = function() {
    this.targetAngle = 180;
    console.log(this.tank.angle, this.targetAngle);
    if (this.tank.rotation !== this.targetAngle) {
        this.tank.angle += 4;
    }
}

Tank.prototype.chase = function() {
    this.targetAngle = Math.atan2(this.enemy.y - this.tank.y, this.enemy.x - this.tank.x);

        //TANK ROTATION
        if (this.tank.rotation !== this.targetAngle) {
            var delta = this.targetAngle - this.tank.rotation;

            if (delta > Math.PI) delta -= Math.PI * 2;
            if (delta < -Math.PI) delta += Math.PI * 2;
            
            if (delta > 0) {
                // Turn clockwise
                this.tank.angle += this.TURN_RATE;
            } else {
                // Turn counter-clockwise
                this.tank.angle -= this.TURN_RATE;
            }
        }

                //TURRET ROTATION
        if (this.turret.rotation !== this.targetAngle) {
            var delta = this.targetAngle - this.turret.rotation;

            if (delta > Math.PI) delta -= Math.PI * 2;
            if (delta < -Math.PI) delta += Math.PI * 2;
            
            if (delta > 0) {
                // Turn clockwise
                this.turret.angle += this.turretTurnRate;
            } else {
                // Turn counter-clockwise
                this.turret.angle -= this.turretTurnRate;
            }
        }
        // Just set angle to target angle if they are close

        if (Math.abs(delta) < this.game.math.degToRad(this.turretTurnRate)) {
            this.turret.rotation = this.targetAngle;
        }
}

Tank.prototype.fire = function() {
    //Fire in the calculated fire rate and only if the turret is pointing at the enemy tank
    if (this.game.time.now > this.nextFire && this.bullets.countDead() > 0 && this.turret.rotation == this.targetAngle) {

        this.nextFire = this.game.time.now + this.fireRate;
        var bullet = this.bullets.getFirstDead();
        bullet.reset(this.turret.x, this.turret.y);
        bullet.rotation = this.game.physics.arcade.moveToObject(bullet, this.enemy, 500);
    }
}

Tank.prototype.kill = function() {
    this.alive = false;
    this.shadow.kill();
    this.tank.kill();
    this.turret.kill();
    this.bullets.destroy();
    this.hpbar.destroy();

    var explosionAnimation = this.explosions.getFirstExists(false);
    explosionAnimation.reset(this.tank.x, this.tank.y);
    explosionAnimation.play('kaboom', 30, false, true);
}

Tank.prototype.update = function() {
    this.shadow.x = this.tank.x;
    this.shadow.y = this.tank.y;
    this.shadow.rotation = this.tank.rotation;

    this.turret.x = this.tank.x;
    this.turret.y = this.tank.y;

    this.rect.x = this.tank.x;
    this.rect.y = this.tank.y;

    this.hpbar.x = Math.floor(this.tank.x);
    this.hpbar.y = Math.floor(this.tank.y);

    if(this.radar()) {
        this.chase();
        this.fire();
    }
    //this.currentSpeed = 100;
    if(this.currentSpeed > 0){
        this.game.physics.arcade.velocityFromRotation(this.tank.rotation, this.currentSpeed, this.tank.body.velocity);
    }
}


var game = new Phaser.Game(1200, 720, Phaser.CANVAS, 'phaser-example', { preload: preload, create: eurecaClientSetup, update: update, render: render });

function preload() {
    game.load.atlas('tank', 'assets/tanks.png', 'assets/tanks.json');
    game.load.atlas('enemy', 'assets/enemy-tanks.png', 'assets/tanks.json');
    game.load.image('logo', 'assets/logo.png');
    game.load.image('bullet', 'assets/bullet.png');
    game.load.image('earth', 'assets/dark_grass.png');
    game.load.spritesheet('kaboom', 'assets/explosion.png', 64, 64, 23);
    game.load.image('radar', 'assets/radar.png');
}





PlayerTank = function(id, game) {
    this.cursor = {
        left:false,
        right:false,
        up:false,
        fire:false      
    }

    this.input = {
        left:false,
        right:false,
        up:false,
        fire:false
    }

    var x = 0;
    var y = 0;

    this.id = id.toString();
    this.game = game;
    this.currentSpeed;
    this.fireRate = 500;
    this.maxHealth = 5;
    this.health = 5;
    this.alive = true;

    this.tank = game.add.sprite(0, 0, 'tank', 'tank1');
    this.shadow = game.add.sprite(0, 0, 'tank', 'shadow');
    this.turret = game.add.sprite(0, 0, 'tank', 'turret');

    this.tank.anchor.setTo(0.5, 0.5);
    this.shadow.anchor.setTo(0.5, 0.5);
    this.turret.anchor.setTo(0.3, 0.5);
    
    game.physics.enable(this.tank, Phaser.Physics.ARCADE);

    this.bullets = game.add.group();
    this.bullets.enableBody = true;
    this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
    this.bullets.createMultiple(30, 'bullet', 0, false);
    this.bullets.setAll('anchor.x', 0.5);
    this.bullets.setAll('anchor.y', 0.5);
    this.bullets.setAll('outOfBoundsKill', true);
    this.bullets.setAll('checkWorldBounds', true);

    this.explosions = game.add.group();

    for (var i = 0; i < 10; i++)
    {
        var explosionAnimation = this.explosions.create(0, 0, 'kaboom', [0], false);
        explosionAnimation.anchor.setTo(0.5, 0.5);
        explosionAnimation.animations.add('kaboom');
    }

    this.tank.immovable = false;
    this.tank.collideWorldBounds = true;
    this.tank.body.bounce.setTo(0, 0);
    this.tank.body.drag.set(0.2);
    this.tank.body.collideWorldBounds = true;
    this.tank.name = id.toString();

    this.style = { font: "15px Arial", fill: "#ff0044", wordWrap: true, wordWrapWidth: this.tank.width, align: "center" };
    this.hpbar = game.add.text(0, 0, this.health.toString() + ' / ' + this.maxHealth.toString(), this.style);
    this.hpbar.anchor.set(1, 1.5);

    this.lastPosition = {x: x, y: y};
    //this.cursors = game.input.keyboard.createCursorKeys();

    game.physics.arcade.velocityFromRotation(this.tank.rotation, 0, this.tank.body.velocity);
}

PlayerTank.prototype.damage = function() {
    this.health -= 1;
    this.hpbar.destroy();
    
    this.hpbar = game.add.text(0, 0, this.health.toString() + ' / ' + this.maxHealth.toString(), this.style);
    this.hpbar.anchor.set(1, 1.5);
    if (this.health <= 0) {
        this.alive = false;

        this.kill();
        return true;
    }
    return false;
}

PlayerTank.prototype.fire = function (target) {

    if (game.time.now > nextFire && this.bullets.countDead() > 0)
    {
        nextFire = game.time.now + this.fireRate;

        bullet = this.bullets.getFirstExists(false);

        bullet.reset(this.turret.x, this.turret.y);

       //bullet.rotation = game.physics.arcade.moveToPointer(bullet, 500, game.input.activePointer);
        bullet.rotation = this.game.physics.arcade.moveToObject(bullet, target, 500);
        
    }

}

PlayerTank.prototype.kill = function () {
   //txt = this.game.add.text(game.camera.width / 2, game.camera.height / 2, "You LOST", {font: "30px Arial", fill: "#ffffff", stroke: '#000000', strokeThickness: 3});
    //txt.anchor.setTo(0.5, 0.5);
    //txt.fixedToCamera = true;

    this.shadow.kill();
    this.tank.kill();
    this.turret.kill();
    this.bullets.destroy();
    this.hpbar.destroy();

    var explosionAnimation = this.explosions.getFirstExists(false);
    explosionAnimation.reset(this.tank.x, this.tank.y);
    explosionAnimation.play('kaboom', 30, false, true);


}

PlayerTank.prototype.update = function() {
    var inputChanged = (
        this.cursor.left != this.input.left ||
        this.cursor.right != this.input.right ||
        this.cursor.up != this.input.up ||
        this.cursor.fire != this.input.fire
    );
    
    
    if (inputChanged)
    {
        //Handle input change here
        //send new values to the server    
        if (this.id == myId)
        {
            // send latest valid state to the server
            this.input.x = this.tank.x;
            this.input.y = this.tank.y;
            this.input.angle = this.tank.angle;
            this.input.rot = this.turret.rotation;
            
            
            eurecaServer.handleKeys(this.input);
            
        }
    }

    //cursor value is now updated by eurecaClient.exports.updateState method
    
    
    if (this.cursor.left)
    {
        this.tank.angle -= 4;
    }
    else if (this.cursor.right)
    {
        this.tank.angle += 4;
    }   
    if (this.cursor.up)
    {
        //  The speed we'll travel at
        this.currentSpeed = 300;
    }
    else
    {
        if (this.currentSpeed > 0)
        {
            this.currentSpeed -= 4;
        }
    }
    if (this.cursor.fire)
    {   
        this.fire({x:this.cursor.tx, y:this.cursor.ty});
    }
    
    
    
    if (this.currentSpeed > 0)
    {
        game.physics.arcade.velocityFromRotation(this.tank.rotation, this.currentSpeed, this.tank.body.velocity);
    }   
    else
    {
        game.physics.arcade.velocityFromRotation(this.tank.rotation, 0, this.tank.body.velocity);
    }
    
    this.shadow.x = this.tank.x;
    this.shadow.y = this.tank.y;
    this.shadow.rotation = this.tank.rotation;

    this.turret.x = this.tank.x;
    this.turret.y = this.tank.y;

    this.hpbar.x = Math.floor(this.tank.x);
    this.hpbar.y = Math.floor(this.tank.y);

   // this.turret.rotation = game.physics.arcade.angleToPointer(this.turret);
};


function create(){
    

    game.world.setBounds(-1000, -1000, 2000, 2000);
    game.stage.disableVisibilityChange  = true;

    land = game.add.tileSprite(0, 0, 1200, 720, 'earth');
    land.fixedToCamera = true;

    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets.createMultiple(100, 'bullet');
    
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 0.5);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);

    enemies = [];

    
    player = new PlayerTank(myId, game);
    tanksList.push(player);
    /*
    for (var i = 0; i < 15; i++) {
        enemies.push(new Tank(i, game, player, enemyBullets));
    }
    */
    explosions = game.add.group();

    for (var i = 0; i < 10; i++)
    {
        var explosionAnimation = explosions.create(0, 0, 'kaboom', [0], false);
        explosionAnimation.anchor.setTo(0.5, 0.5);
        explosionAnimation.animations.add('kaboom');
    }

    cursors = game.input.keyboard.createCursorKeys();

    game.camera.follow(player.tank);
    game.camera.deadzone = new Phaser.Rectangle(250, 250, 500, 300);
    game.camera.focusOnXY(0, 0);
}



function update () {
    if (!ready) return;

    player.input.left = cursors.left.isDown;
    player.input.right = cursors.right.isDown;
    player.input.up = cursors.up.isDown;
    player.input.fire = game.input.activePointer.isDown;
    player.input.tx = game.input.x+ game.camera.x;
    player.input.ty = game.input.y+ game.camera.y;
    
    
    
    player.turret.rotation = game.physics.arcade.angleToPointer(player.turret);

    

    land.tilePosition.x = -game.camera.x;
    land.tilePosition.y = -game.camera.y;

    for (i in tanksList) {
        if(!tanksList[i]) continue;
        tanksList[i].update();
    }
    
   //game.physics.arcade.overlap(enemyBullets, player.tank, bulletHitPlayer, null, player);
    //enemiesAlive = 0;

    for (i in tanksList)
    {   
        for(j in tanksList){
            if(j != i){
                //console.log(j, i);
                game.physics.arcade.collide(tanksList[j].tank, tanksList[i].tank);

            }
        }
        /*
        if (enemies[i].alive)
        {
            enemiesAlive++;
            if(game.physics.arcade.collide(player.tank, enemies[i].tank)){
                enemies[i].kill();
                player.kill();  
            }
            game.physics.arcade.overlap(player.bullets, enemies[i].tank, bulletHitEnemy, null, player);
            if(game.physics.arcade.overlap(player.bullets, enemies[i].rect)){
                enemies[i].dodge();
                enemies[i].signal = true;
            }

            enemies[i].update();
        }
        */
    }   
}

/*

function bulletHitPlayer (tank, bullet) {

    bullet.kill();
    var destroyed = player.damage();

    if (destroyed)
    {
        var explosionAnimation = explosions.getFirstExists(false);
        explosionAnimation.reset(player.tank.x, player.tank.y);
        explosionAnimation.play('kaboom', 30, false, true);
    }
}

function bulletHitEnemy (tank, bullet) {

    bullet.kill();

    var destroyed = enemies[tank.name].damage();

    if (destroyed)
    {
        var explosionAnimation = explosions.getFirstExists(false);
        explosionAnimation.reset(tank.x, tank.y);
        explosionAnimation.play('kaboom', 30, false, true);
    }

}

function signal(enemy) {
    enemy.signal = true;
}

*/

function render(){

}